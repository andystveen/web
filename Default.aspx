﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        </div>
    <br>
    <br>
   
     <!-- Our  Glasses section -->
      <div class="glasses">
         <div class="container">
            <div class="row">
               <div class="col-md-10 offset-md-1">
                  <div class="titlepage">
                     <h2>MODELOS DE LENTES</h2>
                     <p>La óptica es una rama de la física que se dedica al estudio de la luz visible: sus propiedades y su comportamiento. También analiza sus eventuales aplicaciones en la vida del ser humano, como es la construcción de instrumentos para detectarla o valerse de ella.
                     </p>
                  </div>
               </div>
            </div>
         </div>
         <div class="container-fluid">
            <div class="row">
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                  <div class="glasses_box">
                     <figure><img src="assets/images/glass1.png" alt="#"/></figure>
                     <h3><span class="blu">$</span>50</h3>
                     <p>LENTES MONOFOCALES</p>
                       <li class="center-button"><asp:HyperLink ID="HyperLink6" class="read_more" runat="server" Text="Comprar" NavigateUrl="~/Reserva.aspx"/></li>
                  </div>

               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                  <div class="glasses_box">
                     <figure><img src="assets/images/glass2.png" alt="#"/></figure>
                     <h3><span class="blu">$</span>40</h3>
                     <p>LENTES BIFOCALES</p>
                        <li class="center-button"><asp:HyperLink ID="HyperLink1" class="read_more" runat="server" Text="Comprar" NavigateUrl="~/Reserva.aspx"/></li>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                  <div class="glasses_box">
                     <figure><img src="assets/images/glass3.png" alt="#"/></figure>
                     <h3><span class="blu">$</span>60</h3>
                     <p>LENTES PROGRESIVOS</p>
                        <li class="center-button"><asp:HyperLink ID="HyperLink2" class="read_more" runat="server" Text="Comprar" NavigateUrl="~/Reserva.aspx"/></li>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                  <div class="glasses_box">
                     <figure><img src="assets/images/glass4.png" alt="#"/></figure>
                     <h3><span class="blu">$</span>50</h3>
                     <p>LENTES DE SOL</p>
                        <li class="center-button"><asp:HyperLink ID="HyperLink3" class="read_more" runat="server" Text="Comprar" NavigateUrl="~/Reserva.aspx"/></li>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                  <div class="glasses_box">
                     <figure><img src="assets/images/glass5.png" alt="#"/></figure>
                     <h3><span class="blu">$</span>40</h3>
                     <p>LENTES POLARIZADOS</p>
                        <li class="center-button"><asp:HyperLink ID="HyperLink4" class="read_more" runat="server" Text="Comprar" NavigateUrl="~/Reserva.aspx"/></li>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                  <div class="glasses_box">
                     <figure><img src="assets/images/glass6.png" alt="#"/></figure>
                     <h3><span class="blu">$</span>30</h3>
                     <p>LENTES BI-CONVEXAS</p>
                        <li class="center-button"><asp:HyperLink ID="HyperLink5" class="read_more" runat="server" Text="Comprar" NavigateUrl="~/Reserva.aspx"/></li>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                  <div class="glasses_box">
                     <figure><img src="assets/images/glass7.png" alt="#"/></figure>
                     <h3><span class="blu">$</span>60</h3>
                     <p>LENTES PLANO CONVEXAS</p>
                    <li class="center-button"><asp:HyperLink ID="HyperLink7" class="read_more" runat="server" Text="Comprar" NavigateUrl="~/Reserva.aspx"/></li>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                  <div class="glasses_box">
                     <figure><img src="assets/images/glass8.png" alt="#"/></figure>
                     <h3><span class="blu">$</span>100</h3>
                     <p>LENTES PROGRESIVOS</p>
                        <li class="center-button"><asp:HyperLink ID="HyperLink8" class="read_more" runat="server" Text="Comprar" NavigateUrl="~/Reserva.aspx"/></li>
                  </div>
               </div>
             
            </div>
         </div>
      </div>
      <!-- end Our  Glasses section -->
       </div>
</asp:Content>
