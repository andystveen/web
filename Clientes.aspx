﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
      </div>
    <!-- about section -->
         <div class="about">
         <div class="container">
            <div class="row d_flex">
               <div class="col-md-5">
                  <div class="about_img">
                     <figure><img src="assets/images/optica6.png" alt="#"/></figure>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="titlepage">
                     <h2>NUESTROS CLIENTES</h2>
                     <p>La óptica es una rama de la física que se dedica al estudio de la luz visible: sus propiedades y su comportamiento. También analiza sus eventuales aplicaciones en la vida del ser humano, como es la construcción de instrumentos para detectarla o valerse de ella.</p>
                      
                        </div>
                   <li class="center-button"><asp:HyperLink ID="HyperLink3" class="read_more" runat="server" Text="Registrarse" NavigateUrl="~/Reserva.aspx"/></li>
                
               </div>
            </div>
         </div>
      </div>
 
      <!-- about section -->
      </div>
</asp:Content>

