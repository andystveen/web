﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     </div>
    <!-- about section -->
         <div class="about">
         <div class="container">
            <div class="row d_flex">
               <div class="col-md-5">
                  <div class="about_img">
                     <figure><img src="assets/images/optica1.png" alt="#"/></figure>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="titlepage">
                     <h2>Quienes Somos</h2>
                     <p>La óptica es una rama de la física que se dedica al estudio de la luz visible: sus propiedades y su comportamiento. También analiza sus eventuales aplicaciones en la vida del ser humano, como es la construcción de instrumentos para detectarla o valerse de ella.</p>
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
      <div class="about">
         <div class="container">
            <div class="row d_flex">
               <div class="col-md-5">
                  <div class="about_img">
                     <figure><img src="assets/images/optica2.png" alt="#"/></figure>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="titlepage">
                     <h2>Misión</h2>
                     <p>Ofrecer un servicio integral mediante la combinación de valores, calidad de atención y asesoría, de manera que éstos satisfagan las necesidades y expectativas de nuestros clientes, produciendo un impacto positivo en sus vidas y crecimiento personal.</p>
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
          <div class="about">
         <div class="container">
            <div class="row d_flex">
               <div class="col-md-5">
                  <div class="about_img">
                     <figure><img src="assets/images/optica3.png" alt="#"/></figure>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="titlepage">
                     <h2>Visión</h2>
                     <p>Ser la empresa de vanguardia que provea la más alta calidad en sus líneas de productos y la adecuada atención visual de cada uno de nuestros clientes a lo largo de sus vidas.</p>
                  </div>
                 
               </div>
            </div>
         </div>
      </div>
      <!-- about section -->
     </div>
</asp:Content>
