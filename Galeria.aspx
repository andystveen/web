﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
         <br>
    <br>
     <br>
    <br>
     <br>
    <!-- Gallery -->
<div class="row">
  <div class="col-lg-4 col-md-12 mb-4 mb-lg-0">
    <img
      src="https://oftalmologiaquito.com/wp-content/uploads/2021/12/DR-RAUL-SALAZAR-CIRUJANO-OFTALMOLOGO-QUITO-OPTICA.jpg"
      class="w-100 shadow-1-strong rounded mb-4"
      alt="Boat on Calm Water"
    />

    <img
      src="https://opticalamarca.com/wp-content/uploads/2021/08/optica-lamarca-gafas.jpg"
      class="w-100 shadow-1-strong rounded mb-4"
      alt="Wintry Mountain Landscape"
    />
  </div>

  <div class="col-lg-4 mb-4 mb-lg-0">
    <img
      src="https://img.freepik.com/foto-gratis/consumidor-optica-femenina-elige-marco-gafas-tienda-optica-seleccion-gafas-optometrista-profesional_266732-4323.jpg?size=626&ext=jpg"
      class="w-100 shadow-1-strong rounded mb-4"
      alt="Mountains in the Clouds"
    />

    <img
      src="https://www.shutterstock.com/shutterstock/photos/1192290277/display_1500/stock-photo-happy-woman-choosing-eyeglasses-while-male-oculist-standing-near-in-ophthalmic-shop-1192290277.jpg"
      class="w-100 shadow-1-strong rounded mb-4"
      alt="Boat on Calm Water"
    />
  </div>

  <div class="col-lg-4 mb-4 mb-lg-0">
    <img
      src="https://www.shutterstock.com/shutterstock/photos/1724831326/display_1500/stock-photo-eyeglasses-closeup-spectacles-in-woman-s-hands-presenting-glasses-zoom-in-1724831326.jpg"
      class="w-100 shadow-1-strong rounded mb-4"
      alt="Waves at Sea"
    />

    <img
      src="https://img.freepik.com/fotos-premium/atenta-optometrista-que-examina-paciente-lampara-hendidura-clinica-oftalmologia-mujer-hermosa-joven-diagnostica-presion-ocular-equipo-oftalmologico-especial_1212-5510.jpg?w=360"
      class="w-100 shadow-1-strong rounded mb-4"
      alt="Yosemite National Park"
    />

  </div>
    
</div>
<!-- Gallery -->
                
</asp:Content>
